from simplesnake.windows.window import Window
import tkinter as tk
from datetime import datetime
import shelve
from simplesnake.constants import *

class SubmitScoreWindow(Window):
    name = "submitScoreWindow"
    
    def __init__(self, master, fsm, score):
        self.master = master
        self.fsm = fsm
        self.score = score
      
        self.saveButton = tk.Button(self.master, text= "Save", command=self._save)
        self.playerName = tk.Label(self.master, text="Player name")
        self.input = tk.Entry(self.master)

    def _save(self):
        print("Submitted score: {}, for player: {}, at {}".format(self.score, self.input.get(), datetime.now()))
        with shelve.open(scores_db) as db:
            db[str(datetime.now())] = (self.input.get(), self.score)

        self.fsm.showDefeatWindow(self.master)

    def show(self):
        self.playerName.pack()
        self.input.pack()
        self.saveButton.pack()
        
    def hide(self):
        self.playerName.pack_forget()
        self.input.pack_forget()
        self.saveButton.pack_forget()
        