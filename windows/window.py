from abc import ABC, abstractmethod

class Window(ABC):
    @abstractmethod
    def show(self):
        pass

    @abstractmethod
    def hide(self):
        pass
