import tkinter as tk
from game_objects.snake import Snake
from utils.point import Point
from utils.direction import Direction
from utils.painter import Painter
import time
import threading
from utils.fruit_factory import createFruit
from simplesnake.windows.window import Window
from simplesnake.utils.collision_handler import *

class GameWindow(Window):
    name = "gameWindow"
    def createElements(self):
        self.painter.drawSnake(self.snake)
        self.painter.drawFruit(self.fruit)
    
    def closeGame(self):
        self.fsm.showSubmitScoreWindow(self.master, self.score)
        self.close = True

    def defaultAction(self):
        self.handleAction(self.snake._dir)
        
    def stop(self,_):
        self.pause = True
        self.hide()
        self.pauseInfoLabel.pack()

    def resume(self,_):
        self.pause = False
        self.pauseInfoLabel.pack_forget()
        self.canv.pack()

    def onEat(self, _):
        self.snake.onEat()
        self.painter.removeFruit()
        self.score += 1
        self.master.title("Score: {}".format(self.score))
        self.fruit = createFruit(self.windowSize, self.snake.getPoints())
        self.painter.drawFruit(self.fruit)

    def handleAction(self, direction):
        if self.snake._legitMove(direction):
            newHead = self.snake.newHead(direction)
            collisionResult = collides(SnakeBox(newHead), self.windowSize, self.snake, self.fruit)
            
            if collisionResult == Collision.HittedWall or collisionResult == Collision.AteItself:
                self.closeGame()
                return

            if collisionResult == Collision.AteFruit:
                self.onEat(None)

            self.snake.move(direction)
            self.painter.drawSnake(self.snake)

    def action(self, direction):
        return lambda _ : self.handleAction(direction)

    #this could be separate class
    def bindActions(self):
        #https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/key-names.html
        self.canv.bind("<Up>", self.action(Direction.Up))
        self.canv.bind("<Down>",self.action(Direction.Down))
        self.canv.bind("<Left>", self.action(Direction.Left))
        self.canv.bind("<Right>",self.action(Direction.Right))
        self.canv.bind("<Escape>", self.stop)
        self.canv.bind("<r>", self.resume) 

    def hide(self):
        self.canv.pack_forget()

    def show(self):
        self.canv.pack()
        self.canv.focus_set()

    #far too much happens here
    def __init__(self, master, fsm):
        self.master = master
        self.fsm = fsm

        self.windowSize = Point(self.master.winfo_width(), self.master.winfo_height())
        self.canv = tk.Canvas(master=self.master,\
                              width=self.windowSize.x,
                              height=self.windowSize.y)
        self.speed = 0.1
        self.score = 0

        self.pause = False
        self.close = False
        self.snake = Snake(self.windowSize/2)
        self.bindActions()
        self.fruit = createFruit(self.windowSize, self.snake.getPoints())

        self.painter = Painter(self.canv)
        self.pauseInfoLabel = tk.Label(self.master, text="---Pause, press r to resume---")
        self.createElements()
        self.master.title("Score: {}".format(self.score))
        self.master.update_idletasks()
        print("created gameWindow")
    
    def run(self):
        while not self.close:
            if not self.pause:
                self.defaultAction()
            time.sleep(self.speed)

    def play(self):
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()
        self.master.mainloop()