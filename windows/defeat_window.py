import tkinter as tk
from simplesnake.windows.window import *

class DefeatWindow(Window):
    name = "defeatWindow"
    def __init__(self, master, fsm):
        self.master = master
        self.fsm = fsm

        self.playAgainButton = tk.Button(self.master, text= "Play again", command=self.startGame)
        self.backToStart = tk.Button(self.master, text="Back to main menu", command=self.backToMain)
        self.exitButton = tk.Button(self.master, text="Exit", command=self.master.destroy)
        
    def startGame(self):
        self.fsm.startGame(self.master)
       
    def backToMain(self): 
        self.fsm.showStart()

    def show(self):
        self.playAgainButton.pack()
        self.backToStart.pack()
        self.exitButton.pack()
        
    def hide(self):
        self.playAgainButton.pack_forget()
        self.backToStart.pack_forget()
        self.exitButton.pack_forget()
        