import tkinter as tk
from .builders.master_builder import MasterBuilder
from simplesnake.windows.window import Window

class StartWindow(Window):
    name = "StartWindow"
    def createElements(self):
        self.playButton = tk.Button(self.master, text= "Play", command=self.startGame)
        self.leaderBoard = tk.Button(self.master, text="Show leaderboard", command=self.showLeaderBoard)
        self.exitButton = tk.Button(self.master, text="Exit", command=self.master.destroy)

    def __init__(self, fsm):
        self.master = MasterBuilder().build()
        self.createElements()
        self.fsm = fsm
        print("created startWindow")

    def showLeaderBoard(self):
        self.fsm.showLeaderBoard(self.master)

    def startGame(self):
        self.fsm.startGame(self.master)

    def show(self):
        self.playButton.pack()
        self.leaderBoard.pack()
        self.exitButton.pack()
        self.master.mainloop()

    def hide(self):
        self.playButton.pack_forget()
        self.leaderBoard.pack_forget()
        self.exitButton.pack_forget()