from simplesnake.windows.window import Window
from simplesnake.constants import *
import shelve
import tkinter as tk

class LeaderBoardWindow(Window):
    name = "leaderBoardWindow"
    
    def __init__(self, master, fsm):
        self.master = master
        self.fsm = fsm
        self.backToStart = tk.Button(self.master, text="Back to main menu", command=self.backToMain)
        self.scores = tk.Listbox(self.master,  width=self.master.winfo_width(), height=self.master.winfo_height()) #this isn't good, need to go back to main menu
        
    def backToMain(self): 
        self.fsm.showStart()

    def getLeaders(self):
        self.scores.delete(0,tk.END)
        with shelve.open(scores_db) as db:
            for position, (date, (name, score)) in enumerate(db.items(), 1):
                output = "Player {}: {} |  {}".format(name, score, date) #fix formatting
                #fix sorting by scores
                print(output)
                self.scores.insert(position, output)

    def show(self):
        self.getLeaders()
        self.backToStart.pack() 
        self.scores.pack()

    def hide(self):
        self.backToStart.pack_forget()
        self.scores.pack_forget()
        