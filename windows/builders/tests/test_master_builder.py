import unittest
import windows.builders.master_builder as mb

class Test_master_builder(unittest.TestCase):
    def test_consturction(self):
        sut = mb.MasterBuilder()
        self.assertEqual(str(sut.master.title()), str("Snake - simple version"))
        sut = sut.build()
        self.assertEqual(640, sut.winfo_width())
        self.assertEqual(480, sut.winfo_height())

    def test_constSize(self):
        sut = mb.MasterBuilder().constSize().build()
        sut.geometry("320x240")

if __name__ == '__main__':
    unittest.main(exit=False)
