import tkinter as tk

class MasterBuilder:
    def __init__(self):
        self.master = tk.Tk()
        self.master.title("Snake - simple version")
        self.master.geometry("640x480")

    def constSize(self):
        self.master.resizable(0, 0)
        return self
        
    def build(self):
        self.master.update_idletasks()
        return self.master

