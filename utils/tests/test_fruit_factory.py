import unittest
from utils.fruit_factory import createFruit
from utils.point import Point

class Test_fruit_factory(unittest.TestCase):
    def test_A(self):
        boardSize = Point(26, 26)
        points = [Point(1,1)]
        result = createFruit(boardSize, points)
        print(result)

    def test_noSpaceLeft(self):
        with self.assertRaises(IndexError) as  context:
            createFruit(Point(0,0), [])
        self.assertEqual(str(context.exception), "No place to put fruit")
        
if __name__ == '__main__':
    unittest.main(exit=False)
