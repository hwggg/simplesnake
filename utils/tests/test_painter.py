import unittest
from utils.painter import Painter
from unittest.mock import Mock
from utils.point import Point
import tkinter as tk

class Test_painter(unittest.TestCase):
    
    def test_drawPoint(self):
        sut = Painter(Mock())
        point = Point(2, 2)
        default_color = "#476042"

        sut.drawPoint(point)
        sut.canv.create_oval.assert_called_with(1,1,3,3, fill=default_color)

    def test_drawRectangle(self):
        sut = Painter(Mock())
        upper_left = Point(0,0)
        lower_right = Point(4,4)
        outline="#fb0"
        fill="#fb0"

        sut.drawRectangle(upper_left, lower_right)
        sut.canv.create_rectangle.assert_called_with(0,0,4,4, outline=outline, fill=fill)

    def test_clear(self):       
        sut = Painter(Mock())

        sut.clear()
        sut.canv.delete.assert_called_with("all")

if __name__ == '__main__':
    unittest.main(exit=False)