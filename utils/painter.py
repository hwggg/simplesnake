from PIL import Image, ImageTk
import os
from simplesnake.constants import *

class Painter:
    def __init__(self, canv):
        self.snake_body = ImageTk.PhotoImage(Image.open(snake_asset))
        self.fruit =  ImageTk.PhotoImage(Image.open(fruit_asset))
        self.snake_body_count = 0
        self.canv = canv

    def removeFruit(self):
        self.canv.delete("fruit")

    def drawFruit(self, fruit):
        self.canv.create_image(fruit.position.x, fruit.position.y, image=self.fruit, tag="fruit")

    def drawSnake(self, snake):
        for num in range(self.snake_body_count+1):
            self.canv.delete("snake_{}".format(num))
            
        self.snake_body_count = 0
           
        for point in snake.getPoints():
            self.canv.create_image(point.x, point.y, image=self.snake_body, tag="snake_{}".format(self.snake_body_count))
            self.snake_body_count += 1
            