from  simplesnake.utils.direction import Direction 

class Point:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)

    def moveTo(self, direct, value):
        if direct == Direction.Up:
            return Point(self.x, self.y - value)
        elif direct == Direction.Down:
            return Point(self.x, self.y + value)
        elif direct == Direction.Left:
            return Point(self.x - value, self.y)
        return Point(self.x + value, self.y)

    def __str__(self):
        return "x: {}, y: {}".format(self.x, self.y)

    def __repr__(self):
        return "Point({}, {})".format(self.x, self.y)

    def __eq__(self, rhs):
        return self.x == rhs.x and self.y == rhs.y

    def __truediv__(self, divisor):
        if divisor:
            return Point(self.x/divisor, self.y/divisor)