from enum import Enum

class Direction(Enum):
        Up = 0
        Down = 1
        Left = 2
        Right = 3

        def isHorizontal(self):
                return self.value > Direction.Down.value

        def isVertical(self):
                return self.value < Direction.Left.value

        def oposite(self):
                if self == Direction.Up: return Direction.Down
                elif self == Direction.Down: return Direction.Up
                elif self == Direction.Left: return Direction.Right
                else: return Direction.Left

        def __eq__(self, rhs):
                return self.value == rhs.value