from simplesnake.utils.point import Point
from simplesnake.game_objects.snake import Snake
from simplesnake.game_objects.fruit import Fruit

class Box:
    def __init__(self, start, size):
        size_x = size.x-1
        size_y = size.y-1
        
        self.corners = [Point(start.x, start.y), Point(start.x + size_x, start.y), 
                        Point(start.x, start.y+size_y), Point(start.x+size_x, start.y + size_y)]

    def collides(self, box):
        for corner in box.corners:
            if corner in self:
                return True
        return False
        
    def __contains__(self, point):
        return self.corners[-1].x >= point.x >= self.corners[0].x and self.corners[-1].y >= point.y >= self.corners[0].y

    def is_inside(self, box):
        leftUpperInside = box.corners[0].x > self.corners[0].x and box.corners[0].y > self.corners[0].y
        rightBottomInside = box.corners[-1].x < self.corners[-1].x and box.corners[-1].y < self.corners[-1].y
        return leftUpperInside and rightBottomInside
        
    def __repr__(self):
        return str("corners: {}".format(self.corners))

class SnakeBox(Box):
    def __init__(self, start):
        Box.__init__(self, start, Point(Snake.size, Snake.size))

class FruitBox(Box):
    def __init__(self, start):
        Box.__init__(self, start, Point(Fruit.size, Fruit.size))