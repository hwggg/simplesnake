from game_objects.fruit import Fruit
from game_objects.snake import Snake
from .point import Point
import random

def createFruit(boardSize, snakePoints):
    border = Fruit.size*2
    snakePoints = snakePoints or [Point(0,0)]
    
    while True:
        position = Point(random.randrange(border, boardSize.x - border), random.randrange(border, boardSize.y - border))
        #maybe this can be refactored with Box classes
        snake_x = [ x  for point in snakePoints for x in range(point.x - Snake.size, point.x + Snake.size)]
        snake_y = [ y  for point in snakePoints for y in range(point.y - Snake.size, point.y + Snake.size)]
        print(len(snake_x), len(snake_y))
        
        if not (position.x in snake_x and position.y in snake_y):
            return Fruit(position)
    