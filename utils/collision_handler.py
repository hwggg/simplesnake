from simplesnake.utils.point import Point
from simplesnake.utils.box import *
from enum import Enum

class  Collision(Enum):
    HittedWall=0
    AteItself=1
    AteFruit=2
    NoCollision=3

def collides(newBox, boundaries, snake, fruit):
    boundaries = Box(Point(0,0), boundaries)
    snakeBoxes = map(SnakeBox, snake.getPoints())
    fruitBox = FruitBox(Point(0,0)) if not fruit else FruitBox(fruit.position)

    if boundaries.is_inside(newBox):
        for box in snakeBoxes:
            if box.collides(newBox):
                return Collision.AteItself
    
        if fruitBox.collides(newBox):
            return Collision.AteFruit
        return Collision.NoCollision
    else:
        return Collision.HittedWall