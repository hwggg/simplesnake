import unittest
from game_objects.fruit import Fruit
from utils.point import Point

class Test_fruit(unittest.TestCase):
    def test_getPoints(self):
        start = Point(0,0)
        sut = Fruit(start)
        start_point, end_point = sut.getPoints()
        self.assertEqual(start_point, start)
        self.assertEqual(end_point, Point(10, 10))
        
if __name__ == '__main__':
    unittest.main(exit=False)
