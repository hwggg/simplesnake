from simplesnake.utils.direction import Direction
from collections import deque
from simplesnake.utils.point import Point
from PIL import Image
from simplesnake.constants import *

class Snake:
    size, size = Image.open(snake_asset).size
   
    def __init__(self, startPoint):
        self._points = deque([startPoint]) 
        self._dir = Direction.Right
        self._expand(3)

    def _head(self):
        return self._points[-1]
    
    def _tail(self):
        return self._points[0]

    def _insertNewTail(self):
        self._addPoint(self._tail().moveTo(self._dir.oposite(), Snake.size), tail=True)

    def _expand(self, times=1):
        if times:
            for _ in range(times):
                self._insertNewTail()

    def _legitMove(self, direction):
        if self._dir == direction:
            return True

        if self._dir.isHorizontal():
            return direction.isVertical()
        return direction.isHorizontal()
      
    def onEat(self, _ = None):
        self._expand()

    def _dropTail(self):
        self._points.popleft()
    
    def autoMove(self):
        self.move(self._dir)

    def newHead(self, direct):
        return self._head().moveTo(direct, Snake.size)
    
    def move(self, direct):
        if self._legitMove(direct):
            self._addPoint(self.newHead(direct), tail = False)
            self._dir = direct
            self._dropTail()

    def _addPoint(self, point, tail):
        if not tail:
            self._points.append(point)
        else:
            self._points.appendleft(point)

    def getPoints(self):
        return list(self._points)