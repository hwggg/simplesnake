import os

root = os.path.dirname(os.path.abspath(__file__))
db_dir = root+"/db/"
scores_db = db_dir+"scores"

assets_dir = root+"/assets/"
snake_asset = assets_dir+"snake.png"
fruit_asset = assets_dir+"fruit.png"