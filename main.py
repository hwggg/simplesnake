from simplesnake.windows.start_window import StartWindow
from fsm.state_machine import StateMachine

if __name__ == "__main__":
    fsm = StateMachine()
    fsm.attach_window(StartWindow(fsm))
    fsm.update_visible(StartWindow.name)