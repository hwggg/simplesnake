from simplesnake.windows.game_window import *
from simplesnake.windows.start_window import * 
from simplesnake.windows.defeat_window import * 
from simplesnake.windows.leaderboard_window import *
from simplesnake.windows.submit_score_window import *

class StateMachine:
    def __init__(self):
        self.windows = dict()
        self.current = None

    def startGame(self, master):
        self.attach_window(GameWindow(master, self))
        self.update_visible(GameWindow.name)
        self.current.play()

    def showStart(self):
        if StartWindow.name in self.windows:
            self.update_visible(StartWindow.name)
        else:
            raise RuntimeError("Start window must be instantiated first!")
            
    def showDefeatWindow(self, master):
        if not DefeatWindow.name in self.windows:
            self.attach_window(DefeatWindow(master, self))

        self.update_visible(DefeatWindow.name)    
    
    def showSubmitScoreWindow(self, master, score):
        if not SubmitScoreWindow.name in self.windows:
            self.attach_window(SubmitScoreWindow(master, self, score))

        self.update_visible(SubmitScoreWindow.name)

    def showLeaderBoard(self, master):
        if not LeaderBoardWindow.name in self.windows:
            self.attach_window(LeaderBoardWindow(master, self))

        self.update_visible(LeaderBoardWindow.name)

    def attach_window(self, window):
        self.windows[window.name] = window

    def update_visible(self, name):
        if not self.current:
            self.current = self.windows[name]
            self.current.show()
            return

        if name != self.current.name:
            self.current.hide()
            self.current = self.windows[name]
            self.current.show()